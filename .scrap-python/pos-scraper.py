from bs4 import BeautifulSoup	
import requests


API_ENDPOINT = "http://ltrc.iiit.ac.in/analyzer/tamil/run.cgi"
  

inputdata = open("tamil-corpus.txt",'r');
inputLines = inputdata.readlines();
outf = open("dataout.txt","w");
outstr = ""
i=0
inputLinestemp = inputLines
for line in inputLinestemp:  
	data = {
		'input':line,
		'notation':'utf',
		'out_notation':'utf', 
        }
 	
	response = requests.post(url = API_ENDPOINT, data = data)
	
	soup = BeautifulSoup(response.text, 'html.parser')
	print(i);
	i+=1 
	tbody = soup.find_all('p');
	tbody = tbody[-2];
	tbody = tbody.table;
	outstr += str(tbody);
	outstr +="\n\n\n****************************************************************\n\n"

outf.write(outstr);
