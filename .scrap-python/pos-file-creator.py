from bs4 import BeautifulSoup
inputFile = open("dataout.txt");
inputText = inputFile.read();
soup = BeautifulSoup(inputText,'html.parser');
rows = soup.find_all('tr');
pos_out = open("pos-out.txt","w")
for row in rows:
	cols = row.find_all('td');
	pos_out_string = ''
	for col in cols:
		pos_out_string += col.string
		pos_out_string +=" "
	pos_out.write(pos_out_string)
	pos_out.write("\n")