def isTitle(word):
    return (word == "செல்வி" or word == "திருமதி" or word == "திரு");


def word2features(doc,i):
    doc[i] = doc[i].strip("\n");
    #print(doc[i]);
    #doc[i] = doc[i].split();
    word = doc[i][0]
    postag = doc[i][1]
    # Common features for all words
    word = str(word)
    #print(postag)
    #print(word)
    features = [
        'bias',
        'word=' + word,
        'word[-3:]=' + word[-3:],
        'word[-2:]=' + word[-2:],
        'word.istitle=%s' % isTitle(word),
        'word.isdigit=%s' % word.isdigit(),
        'postag=' + postag
    ]

    # Features for words that are not
    # at the beginning of a document
    if i > 0:
        word1 = doc[i-1][0]
        postag1 = doc[i-1][1]
        features.extend([
            '-1:word=' + word1,
            '-1:word.istitle=%s' % isTitle(word1),
            '-1:word.isdigit=%s' % word1.isdigit(),
            '-1:postag=' + postag1
        ])
    else:
        # Indicate that it is the 'beginning of a document'
        features.append('BOS')

    # Features for words that are not
    # at the end of a document
    if i < len(doc)-1:
        word1 = doc[i+1][0]
        postag1 = doc[i+1][1]
        features.extend([
            '+1:word.lower=' + word1.lower(),
            '+1:word.istitle=%s' % isTitle(word1),
            '+1:word.isdigit=%s' % word1.isdigit(),
            '+1:postag=' + postag1
        ])
    else:
        # Indicate that it is the 'end of a document'
        features.append('EOS')

    return features

# A function for extracting features in documents
def extract_features(doc):
    #doc = doc.split("\n");
    #print(doc);
    return [word2features(doc,i) for i in range(len(doc))]

# A function fo generating the list of labels for each document
def get_labels(doc):
    #doc = doc.split("\n")
    ylis = []
    for x in doc:
        x = x.split(" ");
        if(len(x) > 2):
            ylis.append(x[2]);
        else:
            ylis.append("other");
    return ylis

