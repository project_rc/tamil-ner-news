from bs4 import BeautifulSoup	
import requests
import sys
import codecs
import pycrfsuite
from word2features import *


API_ENDPOINT = "http://ltrc.iiit.ac.in/analyzer/tamil/run.cgi"
datapath = "../dataset2/"  

inputdata = input();
inputdata = "1 " + inputdata
#inputLines = inputdata.readlines();
#outf = open(datapath + sys.argv[2],"w");
outstr = ""
i=0
inputLinestemp = [inputdata]
for line in inputLinestemp:  
	data = {
		'input':line,
		'notation':'utf',
		'out_notation':'utf', 
        }
 	
	response = requests.post(url = API_ENDPOINT, data = data)
	
	soup = BeautifulSoup(response.text, 'html.parser')
	#print(i);
	i+=1 
	tbody = soup.find_all('p');
	tbody = tbody[-2];
	tbody = tbody.table;
	outstr += str(tbody);
	outstr +="\n\n\n****************************************************************\n\n"

#print(outstr)
inputText = outstr;
soup = BeautifulSoup(inputText,'html.parser');
rows = soup.find_all('tr');
pos_out = ""
for i in range(0,len(rows)):
	pos_out_str = "";
	#print(cols[i].string)
	cols = rows[i].find_all("td");
	if(len(cols) == 1):
		if (cols[0].string).find("Sentence") > -1:
			pos_out_str += "newline\n";
	elif len(cols) == 4:
		if not cols[1].string.find("((") > -1 and not cols[1].string.find("))") > -1:
			pos_out_string = ""
			pos_out_string += cols[1].string.strip();
			pos_out_string += " ";
			pos_out_string += cols[2].string.strip();
			print(pos_out_string)
			pos_out_string += "\n";
			pos_out_str += (pos_out_string);
	pos_out += pos_out_str
print(pos_out)
datapath = "../dataset2/"
modelpath = "../models/"
outputpath = "../outputs/"

#Generate predictions
#test  = open(datapath + sys.argv[2],"r",encoding = "utf-8");
test = pos_out.split("\n");
print(test)
testdata = []
testdatarow = []

for d in test:
    if d.find("newline") >= 0:
        testdata.append(testdatarow);
        testdatarow = []
    else:
        testdatarow.append(d)

tagger = pycrfsuite.Tagger()
tagger.open(modelpath + sys.argv[1])
#write_file = open(outputpath  + sys.argv[3],'w',encoding="utf-8")
test_data = []
a = 0
"""for tes in testda:
    tes[:] = [item for item in tes if item != '']
    sentence = []
    for te in tes:
        te = te.split(" ")
        sentence.append(tuple(te))
    test_data.append([sentence,"newline"])"""

#print(test_data[0])

for doc in testdata:
    X_test = [extract_features(doc)]
    y_pred = [tagger.tag(xseq) for xseq in X_test][0]
    print(doc, y_pred)
    i = 0
    for y_p in y_pred:
        print(doc[i] + " " + y_p + "\n")
        i+=1
    #write_file.write("newline" + '\n')


