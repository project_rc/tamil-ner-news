import pycrfsuite
from word2features import *
import sys
#test = open("/home/mdp/PycharmProjects/FInal_NER_Task/arnekt-iecsil-baseline_task_a/data/arnekt-iecsil-ie-corpus_test_1/task_a/v1_test1.te", "r").read().split("newline")
#test[:] = [item for item in test if item != '']
#test_data = []
datapath = "../dataset2/"
modelpath = "../models/"
outputpath = "../outputs/"

#Generate predictions
test  = open(datapath + sys.argv[2],"r",encoding = "utf-8");
test = test.readlines();
testda = []
dis = []

for d in test:
    if d.find("newline") >= 0:
        testda.append(dis);
        dis = []
    else:
        dis.append(d)
tagger = pycrfsuite.Tagger()
tagger.open(modelpath + sys.argv[1])
write_file = open(outputpath  + sys.argv[3],'w',encoding="utf-8")
test_data = []

"""for tes in testda:
    tes[:] = [item for item in tes if item != '']
    sentence = []
    for te in tes:
        te = te.split(" ")
        sentence.append(tuple(te))
    test_data.append([sentence,"newline"])"""

#print(test_data[0])

for doc in testda:
    X_test = [extract_features(doc)]
    y_pred = [tagger.tag(xseq) for xseq in X_test][0]
    print(doc, y_pred)
    i = 0
    for y_p in y_pred:
        write_file.write(doc[i] + " " + y_p + '\n')
        i+=1
    write_file.write("newline" + '\n')
