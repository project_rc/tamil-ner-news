from bs4 import BeautifulSoup	
import requests
import sys

API_ENDPOINT = "http://ltrc.iiit.ac.in/analyzer/tamil/run.cgi"
datapath = "../dataset2/"  

inputdata = open(datapath + sys.argv[1],'r');
inputLines = inputdata.readlines();
outf = open(datapath + sys.argv[2],"w");
outstr = ""
i=0
inputLinestemp = inputLines
for line in inputLinestemp:  
	data = {
		'input':"hi " + line,
		'notation':'utf',
		'out_notation':'utf', 
        }
 	
	response = requests.post(url = API_ENDPOINT, data = data)
	
	soup = BeautifulSoup(response.text, 'html.parser')
	print(i);
	i+=1 
	tbody = soup.find_all('p');
	tbody = tbody[-2];
	tbody = tbody.table;
	outstr += str(tbody);
	outstr +="\n\n\n****************************************************************\n\n"

outf.write(outstr);
